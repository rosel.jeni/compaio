import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { AboutComponent } from './about.component';
import { AuthenticationGuard } from '../shared/guard/authentication.guard';

@NgModule({
    imports: [CommonModule, SharedModule],
    declarations: [AboutComponent],
    exports: [AboutComponent],
    providers: [AuthenticationGuard]
})
export class AboutModule { }
