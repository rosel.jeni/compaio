import { Route } from '@angular/router';
import { AboutComponent } from './index';
import { AuthenticationGuard } from '../shared/guard/authentication.guard';

export const AboutRoutes: Route[] = [
  {
    path: 'about',
    component: AboutComponent,
    canActivate: [AuthenticationGuard]
  }
];
