import { Route } from '@angular/router';
import { HomeComponent } from './index';
import { AuthenticationGuard } from '../shared/guard/authentication.guard';

export const HomeRoutes: Route[] = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthenticationGuard]
  }
];
