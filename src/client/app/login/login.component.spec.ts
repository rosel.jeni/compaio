import { Component } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { async } from '@angular/core/testing';
import { HttpModule } from '@angular/http';

import { LoginModule } from './login.module';
import { LocationStrategy } from '@angular/common';
import { GlobalEventsManager } from '../shared/guard/global-events.manager';
import { AuthenticationService } from '../shared/services/authentication/authentication.service';
import { Observable } from 'rxjs/Rx';
import any = jasmine.any;

class MockRouter {
  navigate = jasmine.createSpy('navigate');
}
const mockRouter = new MockRouter();

class MockActivatedRoute {}
class MockLocationStrategy {}
class MockAuthenticationService {
  login = jasmine.createSpy('login');
  logout = jasmine.createSpy('logout');
}
const mockAuthenticationService = new MockAuthenticationService();

export function main() {
  describe('Login component', () => {
    // setting module for testing
    // Disable old forms
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FormsModule, RouterModule, HttpModule, LoginModule],
        declarations: [TestComponent],
        providers: [
          GlobalEventsManager,
          {provide: Router, useValue: mockRouter},
          {provide: LocationStrategy, useClass: MockLocationStrategy},
          {provide: ActivatedRoute, useClass: MockActivatedRoute},
          {provide: AuthenticationService, useValue: mockAuthenticationService},
        ]
      });
    });

    it('should navigate to home on successful login',
      async(() => {
        TestBed
          .compileComponents()
          .then(() => {
            let fixture = TestBed.createComponent(TestComponent);
            fixture.detectChanges();

            let loginInstance = fixture.debugElement.children[0].componentInstance;
            let loginDOMEl = fixture.debugElement.children[0].nativeElement;

            expect(loginDOMEl.querySelector('input[name=username]').type).toEqual('text');
            expect(loginDOMEl.querySelector('input[name=username]').value).toEqual('');
            expect(loginDOMEl.querySelector('input[name=password]').type).toEqual('password');
            expect(loginDOMEl.querySelector('input[name=password]').value).toEqual('');

            loginInstance.username = 'Jenisa';
            loginInstance.password = 'Rosel';

            const successfulObservable = new Observable<boolean>((observer: any) => {
              observer.next(true);
              observer.complete();
            });

            mockAuthenticationService.login.and.returnValue(successfulObservable);

            expect(loginInstance.login()).toBeFalsy();
            expect(mockRouter.navigate).toHaveBeenCalledWith(['/']);
          });
      }));

    it('should set error message if failed login',
      async(() => {
        TestBed
          .compileComponents()
          .then(() => {
            let fixture = TestBed.createComponent(TestComponent);
            fixture.detectChanges();

            let loginInstance = fixture.debugElement.children[0].componentInstance;

            loginInstance.username = 'Jenisa';
            loginInstance.password = 'Rosel';

            const failedObservable = new Observable<boolean>((observer: any) => {
              observer.next(false);
              observer.error(any);
              observer.complete();
            });

            mockAuthenticationService.login.and.returnValue(failedObservable);
            expect(loginInstance.login()).toBeFalsy();
            expect(loginInstance.errorMessage).toMatch('Username or password is incorrect.');
          });
      }));

    it('should redirect to registration page when gotoRegister clicked',
      async(() => {
        TestBed
          .compileComponents()
          .then(() => {
            let fixture = TestBed.createComponent(TestComponent);
            fixture.detectChanges();

            let mockAuthenticationService = fixture.debugElement.children[0].componentInstance;
            mockAuthenticationService.gotoRegister();
            expect(mockRouter.navigate).toHaveBeenCalledWith(['/register']);
          });
      }));
  });
}

@Component({
  selector: 'test-cmp',
  template: '<sd-login></sd-login>'
})
class TestComponent { }
