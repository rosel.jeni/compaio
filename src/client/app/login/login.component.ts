import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../shared/services/authentication/index';
import { Router } from '@angular/router';
import { GlobalEventsManager } from '../shared/guard/global-events.manager';

/**
 * This class represents the lazy loaded LoginComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css'],
})

export class LoginComponent implements OnInit {
  username: string = '';
  password: string = '';
  errorMessage: string = '';

  /**
   * Creates an instance of the LoginComponent with the injected
   * AuthenticationService.
   *
   * @param {AuthenticationService} AuthenticationService - The injected NameListService.
   */
  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private globalEventsManager: GlobalEventsManager) {
  }

  ngOnInit() {
    this.globalEventsManager.showNavBar.emit(false);
    this.authenticationService.logout();
  }

  login(): boolean {
    this.authenticationService.login(this.username, this.password)
      .subscribe(
        result => {
          this.globalEventsManager.showNavBar.emit(true);
          this.router.navigate(['/']);
        },
        error => this.errorMessage = 'Username or password is incorrect.'
      );

    return false;
  }

  gotoRegister(): void {
    this.router.navigate(['/register']);
  }
}
