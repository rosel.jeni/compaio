import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { LoginComponent } from './login.component';
import { AuthenticationService } from '../shared/services/authentication/authentication.service';
import { RouterModule } from '@angular/router';
import { GlobalEventsManager } from '../shared/guard/global-events.manager';

@NgModule({
  imports: [CommonModule, SharedModule, RouterModule],
  declarations: [LoginComponent],
  exports: [LoginComponent, RouterModule],
  providers: [AuthenticationService, GlobalEventsManager]
})
export class LoginModule { }
