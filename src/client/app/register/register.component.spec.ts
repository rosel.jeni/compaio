import { Component } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { async } from '@angular/core/testing';
import { HttpModule } from '@angular/http';

import { RegisterModule } from './register.module';
import { RegistrationService } from '../shared/services/registration/registration.service';
import { LocationStrategy } from '@angular/common';
import { Observable } from 'rxjs/Rx';
import any = jasmine.any;

class MockRouter {
  navigate = jasmine.createSpy('navigate');
}
const mockRouter = new MockRouter();
class MockActivatedRoute {}
class MockLocationStrategy {}

class MockRegistrationService {
  register = jasmine.createSpy('register');
}
const mockRegistrationService = new MockRegistrationService();

export function main() {
  describe('Register component', () => {
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FormsModule, RouterModule, HttpModule, RegisterModule],
        declarations: [TestComponent],
        providers: [
          {provide: Router, useValue: mockRouter},
          {provide: LocationStrategy, useClass: MockLocationStrategy},
          {provide: RegistrationService, useValue: mockRegistrationService} ,
          {provide: ActivatedRoute, useClass: MockActivatedRoute}
        ]
      });
    });

    it('should display success message on successful registration',
      async(() => {
        TestBed
          .compileComponents()
          .then(() => {
            let fixture = TestBed.createComponent(TestComponent);
            fixture.detectChanges();

            let registerInstance = fixture.debugElement.children[0].componentInstance;
            let registerDOMEl = fixture.debugElement.children[0].nativeElement;

            expect(registerDOMEl.querySelector('input[name=username]').type).toEqual('text');
            expect(registerDOMEl.querySelector('input[name=username]').value).toEqual('');
            expect(registerDOMEl.querySelector('input[name=password]').type).toEqual('password');
            expect(registerDOMEl.querySelector('input[name=password]').value).toEqual('');

            registerInstance.username = 'Jenisa';
            registerInstance.password = 'Rosel';

            const successfulObservable = new Observable<boolean>((observer: any) => {
              observer.next(true);
              observer.complete();
            });

            mockRegistrationService.register.and.returnValue(successfulObservable);
            expect(registerInstance.register()).toBeFalsy();
            expect(registerInstance.notificationMessage).toMatch('User successfully added.');
          });
      }));

    it('should display error message on failed registration',
      async(() => {
        TestBed
          .compileComponents()
          .then(() => {
            let fixture = TestBed.createComponent(TestComponent);
            fixture.detectChanges();

            let registerInstance = fixture.debugElement.children[0].componentInstance;

            const failedObservable = new Observable<boolean>((observer: any) => {
              observer.next(false);
              observer.error(any);
              observer.complete();
            });

            mockRegistrationService.register.and.returnValue(failedObservable);
            expect(registerInstance.register()).toBeFalsy();
            expect(registerInstance.notificationMessage).toBe(any, any);
          });
      }));

    it('should redirect to login page when gotoLogin clicked',
      async(() => {
        TestBed
          .compileComponents()
          .then(() => {
            let fixture = TestBed.createComponent(TestComponent);
            fixture.detectChanges();

            let registerInstance = fixture.debugElement.children[0].componentInstance;
            registerInstance.gotoLogin();
            expect(mockRouter.navigate).toHaveBeenCalledWith(['/login']);
          });
      }));
  });
}

@Component({
  selector: 'test-cmp',
  template: '<sd-register></sd-register>'
})
class TestComponent { }
