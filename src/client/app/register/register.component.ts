import { Component } from '@angular/core';
import { RegistrationService } from '../shared/services/registration/index';
import { Router } from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'sd-register',
  templateUrl: 'register.component.html',
  styleUrls: ['register.component.css'],
})

export class RegisterComponent {

  username: string;
  password: string;
  notificationMessage: string = '';

  constructor(public registrationService: RegistrationService,
              private router: Router) {}

  register(): boolean {
    this.registrationService.register(this.username, this.password)
      .subscribe(
        result  => this.notificationMessage = 'User successfully added.',
        error   => this.notificationMessage = <any>error
      );
    return false;
  }

  gotoLogin(): void {
    this.router.navigate(['/login']);
  }
}
