import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { RegisterComponent } from './register.component';
import { RegistrationService } from '../shared/services/registration/registration.service';

@NgModule({
  imports: [CommonModule, SharedModule],
  declarations: [RegisterComponent],
  exports: [RegisterComponent],
  providers: [RegistrationService]
})
export class RegisterModule { }
