/**
 * This barrel file provides the exports for the shared resources (services, components).
 */
export * from './services/name-list/index';
export * from './templates/navbar/index';
export * from './templates/toolbar/index';
export * from './config/env.config';
