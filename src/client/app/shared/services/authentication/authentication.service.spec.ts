import { ReflectiveInjector } from '@angular/core';
import { BaseRequestOptions, ConnectionBackend, Http, Response, ResponseOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { Observable } from 'rxjs/Rx';

import { AuthenticationService } from './authentication.service';

export function main() {
  describe('Authentication Service', () => {
    let authenticationService: AuthenticationService;
    let mockBackend: MockBackend;
    let response: any;
    let connection: any;

    beforeEach(() => {

      let injector = ReflectiveInjector.resolveAndCreate([
        AuthenticationService,
        BaseRequestOptions,
        MockBackend,
        {provide: Http,
          useFactory: function(backend: ConnectionBackend, defaultOptions: BaseRequestOptions) {
            return new Http(backend, defaultOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        },
      ]);
      authenticationService = injector.get(AuthenticationService);
      mockBackend = injector.get(MockBackend);

      mockBackend.connections.subscribe((c: any) => connection = c);
      response = authenticationService.login('jen', 'jen');
    });


    it('should return an Observable when login called', () => {
      expect(response).toEqual(jasmine.any(Observable));
    });

    it('should resolve to true when login successful', () => {
      connection.mockRespond(new Response(
        new ResponseOptions({ status: 200, body: { token: 'fake-jwt-token' } })
      ));

      let result: any;
      response.subscribe((data: any) => result = data);
      expect(result).toEqual(true);
    });

    it('should resolve to false when login fails', () => {
      connection.mockRespond(new Response(
        new ResponseOptions({ status: 400 })
      ));

      let result: any;
      response.subscribe((data: any) => result = data);
      expect(result).toEqual(false);
    });
  });
}
