import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Config } from '../../index';

/**
 * This class provides the Authentication service with methods to authenticate the user.
 */
@Injectable()
export class AuthenticationService {
  public token: string;
  /**
   * Creates a new AuthenticationService with the injected Http.
   * @param {Http} http - The injected Http.
   * @constructor
   */
  constructor(private http: Http) {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.token = currentUser && currentUser.token;
  }

  /**
   * Returns an Observable for the HTTP GET request for the JSON resource.
   * @return {boolean} The Observable for the HTTP request.
   */
  login(username: string, password: string): Observable<boolean> {
    let body = JSON.stringify({ username, password });
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(`${Config.API}/api/login`, body, options)
      .map((response: Response) => {
        let token = response.json() && response.json().token;
        if (token) {
          this.token = token;
          localStorage.setItem('currentUser', JSON.stringify({username: username, token: token}));
        }
        return response.ok;
      })
      .catch(this.handleError);
  }

  /**
   * Removes user from local storage and clear token.
   */
  logout() :void {
    this.token = null;
    localStorage.removeItem('currentUser');
  }
  /**
   * Handle HTTP error
   */
  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return Observable.throw(errMsg);
  }
}

