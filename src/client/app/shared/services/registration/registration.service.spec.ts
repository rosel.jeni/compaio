import { ReflectiveInjector } from '@angular/core';
import { BaseRequestOptions, ConnectionBackend, Http, Response, ResponseOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { Observable } from 'rxjs/Rx';
import { RegistrationService } from './registration.service';

export function main() {
  describe('Registration Service', () => {
    let registrationService: RegistrationService;
    let mockBackend: MockBackend;
    let response: any;
    let connection: any;

    beforeEach(() => {

      let injector = ReflectiveInjector.resolveAndCreate([
        RegistrationService,
        BaseRequestOptions,
        MockBackend,
        {provide: Http,
          useFactory: function(backend: ConnectionBackend, defaultOptions: BaseRequestOptions) {
            return new Http(backend, defaultOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        },
      ]);
      registrationService = injector.get(RegistrationService);
      mockBackend = injector.get(MockBackend);

      mockBackend.connections.subscribe((c: any) => connection = c);
      response = registrationService.register('jen', 'jen');
    });

    it('should return an Observable when registration called', () => {
      expect(response).toEqual(jasmine.any(Observable));
    });

    it('should resolve to true when registration is successful', () => {
      connection.mockRespond(new Response(
        new ResponseOptions({ status: 200 })
      ));

      let result: any;
      response.subscribe((data: any) => result = data);
      expect(result).toEqual(true);
    });

    it('should resolve to false when registration fails', () => {
      connection.mockRespond(new Response(
        new ResponseOptions({ status: 400 })
      ));

      let result: any;
      response.subscribe((data: any) => result = data);
      expect(result).toEqual(false);
    });
  });
}
