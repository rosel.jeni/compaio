import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { ToolbarComponent } from './templates/toolbar/index';
import { NavbarComponent } from './templates/navbar/index';
import { NameListService } from './services/name-list/index';
import { RegistrationService } from './services/registration/index';
import { AuthenticationService } from './services/authentication/authentication.service';
import { GlobalEventsManager } from './guard/global-events.manager';

@NgModule({
  imports: [CommonModule, RouterModule],
  declarations: [ToolbarComponent, NavbarComponent],
  exports: [ToolbarComponent, NavbarComponent,
    CommonModule, FormsModule, RouterModule]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [NameListService, RegistrationService, AuthenticationService, GlobalEventsManager]
    };
  }
}
