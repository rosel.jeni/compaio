import { Component } from '@angular/core';
import { GlobalEventsManager } from '../../guard/global-events.manager';

/**
 * This class represents the navigation bar component.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-navbar',
  templateUrl: 'navbar.component.html',
  styleUrls: ['navbar.component.css'],
})

export class NavbarComponent {
  showNavBar: boolean = false;
  constructor(private globalEventsManager: GlobalEventsManager) {
    this.globalEventsManager.showNavBar.subscribe((mode: boolean) => {
      this.showNavBar = mode;
    });
  }
}
