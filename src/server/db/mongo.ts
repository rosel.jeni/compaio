import * as mongoose from 'mongoose';

/**
 * Init Names List.
 */
export function Init() {

//
// Connect to database
// -----------------------------------------------------------------------------
//  mongoose.Promise = require('bluebird');
  const databaseUrl = process.env.DATABASE_URL || 'mongodb://localhost/seed';
  mongoose.connect(databaseUrl);
}
