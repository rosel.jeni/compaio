import * as mongoose from 'mongoose';
const passportLocalMongoose = require('passport-local-mongoose');

const modelName = 'User';

const schema = new mongoose.Schema({ });

schema.plugin(passportLocalMongoose);

export default mongoose.model(modelName, schema);
