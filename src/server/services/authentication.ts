import * as express from 'express';
import * as User from '../db/schema/user';

import * as passport from 'passport';
import * as expressJwt from 'express-jwt';
import * as jwt from 'jsonwebtoken';
import * as cookieParser from 'cookie-parser';


export function authentication(app: express.Application) {

  const jwtAuthSecret = 'Jenisa Secret';

  app.use(cookieParser());

  app.use(expressJwt({
    secret: jwtAuthSecret,
    credentialsRequired: false,
    getToken: req => req.cookies.id_token,
  }));
  app.use(passport.initialize());
  passport.use((User as any).default.createStrategy());
  passport.serializeUser((User as any).default.serializeUser());
  passport.deserializeUser((User as any).default.deserializeUser());

  app.post('/api/login',
    passport.authenticate('local'),
    (req, res) => {
      const expiresIn = 60 * 60 * 24 * 180; // 180 days
      const token = jwt.sign(req.user, jwtAuthSecret, { expiresIn });
      //res.cookie('id_token', token, { maxAge: 1000 * expiresIn, httpOnly: true });
      res.json({
        token,
        expiresIn
      });
    }
  );

  app.get('/api/logout', (req, res) => {
    req.logout();
    res.redirect('/login');
  });

};
