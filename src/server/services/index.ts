import * as express from 'express';
import { registration } from './registration';
import { nameList } from './name.list';
import {authentication} from "./authentication";

export function init(app: express.Application) {
    authentication(app);
    registration(app);
    nameList(app);
}
