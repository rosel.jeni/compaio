import * as express from 'express';
import * as User from '../db/schema/user';

export function registration(app: express.Application) {
  app.post('/api/register',
    (req, res, next) => {
      (User as any).default.register({ username: req.body.username }, req.body.password, (err: any) => {
        if (err) {
          err.status = 422;
          return next(err);
        }

        res.sendStatus(200);
      });
    });
};
